#[macro_use]
extern crate fomat_macros;

use std::env::var;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

// To refresh the ABI:
//
//   bindgen --no-layout-tests sqlite3.h -- -D SQLITE_ENABLE_SESSION > sqlite3.rs
//   rustfmt sqlite3.rs

// To build the command-line version:
//
//   wget https://www.sqlite.org/2020/sqlite-autoconf-3310100.tar.gz
//   tar -xzf sqlite-autoconf-3310100.tar.gz
//   cd sqlite-autoconf-3310100/
//   apt install libreadline6-dev
//   ./configure --enable-fts5 --enable-session --enable-json1 --enable-readline
//   make && make install && make clean && ldconfig

/// A folder cargo creates for our build.rs specifically.
fn out_dir() -> PathBuf {
    // cf. https://github.com/rust-lang/cargo/issues/3368#issuecomment-265900350
    let out_dir = var("OUT_DIR").expect("!OUT_DIR");
    let out_dir = Path::new(&out_dir);
    if !out_dir.is_dir() {
        panic!("OUT_DIR !is_dir")
    }
    out_dir.to_path_buf()
}

/// Downloads a file, placing it into the given path
/// and sharing the download status on the standard error stream.
///
/// Panics on errors.
///
/// The idea is to replace wget and cURL build dependencies, particularly on Windows.
/// Being able to see the status of the download in the terminal
/// seems more important here than the Future-based parallelism.
fn hget(url: &str, to: PathBuf) {
    eprintln!("hget] Downloading {} ...", url);
    let file = fs::File::create(&to).expect("!create");
    let resp = attohttpc::get(url).send().expect("!get");
    if !resp.is_success() {
        panic!("!is_success")
    }
    let _size = resp.write_to(file).expect("!write_to");
}

fn build_sqlite() {
    let out_dir = out_dir();
    if out_dir.join("libsqlite3.a").exists() {
        return;
    }
    if out_dir.join("sqlite3.lib").exists() {
        return;
    }

    let zip = out_dir.join("sqlite-amalgamation-3310100.zip");
    if !zip.exists() {
        // Fetching amalgamation (cf. https://www.sqlite.org/download.html).
        let zip_url = "https://www.sqlite.org/2020/sqlite-amalgamation-3310100.zip";
        let tmp = out_dir.join("sqlite-amalgamation-3310100.zip.tmp");
        hget(zip_url, tmp.clone());
        fs::rename(tmp, &zip).expect("!rename")
    }

    let zip = fs::File::open(&zip).expect("!open");
    let mut zip = zip::ZipArchive::new(zip).expect("!zip");
    for i in 0..zip.len() {
        let mut zfile = zip.by_index(i).expect("!zfile");
        if zfile.is_dir() {
            continue;
        }
        let to = Path::new(zfile.name());
        let to = to.file_name().unwrap().to_str().unwrap();
        if to != "sqlite3.c" {
            continue;
        } // Only unpack the sqlite3.c
        let to = out_dir.join(to);
        epintln! ("ᵘ" [to] "…");
        let mut to = fs::File::create(to).expect("!create");
        io::copy(&mut zfile, &mut to).expect("!copy");
    }

    cc::Build::new()
        .file(out_dir.join("sqlite3.c"))
        // NB: We also need the pre-update hook in order to see the session symbols.
        // #if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)
        .define("SQLITE_ENABLE_PREUPDATE_HOOK", None)
        .define("SQLITE_ENABLE_SESSION", None)
        // https://www.sqlite.org/compile.html
        .define("SQLITE_DEFAULT_MEMSTATUS", Some("0"))
        .define("SQLITE_ENABLE_JSON1", None)
        .define("SQLITE_DQS", Some("0"))
        .opt_level(2)
        .pic(true)
        .compile("sqlite3")
}

fn main() {
    build_sqlite();
    println!("cargo:rustc-link-search=native={}", out_dir().display());
    println!("cargo:rustc-link-lib=sqlite3")
}
