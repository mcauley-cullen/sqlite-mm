#![feature(test)]
extern crate test;

use gstuff::now_float;
use sqlite_mm::{ChangesetEn, ChangesetEnId, ReplicatedSQLite, Replication, ReplicationArc};
use std::sync::Arc;
use std::thread;
use std::time::Duration;

pub struct NopRepl {}
impl Replication for NopRepl {
    fn publish(&self, _changesets: &Vec<ChangesetEn>) -> Result<Vec<ChangesetEnId>, String> {
        Ok(Vec::new())
    }
}

#[test]
#[should_panic(expected = "write_wait cb panic: ups")]
fn write_wait_panic() {
    let repl = ReplicationArc::from(NopRepl {});
    let db = Arc::new(ReplicatedSQLite::init(":memory:", |_, _| {}, repl).unwrap());
    let _ = db.write_wait(9., |_db| {
        if 1 == 1 {
            panic!("ups")
        }
        Ok(())
    });
}

#[test]
fn write_wait_timeout() {
    let repl = ReplicationArc::from(NopRepl {});
    let db = Arc::new(ReplicatedSQLite::init(":memory:", |_, _| {}, repl).unwrap());

    // Timeout only affects the queueus.
    // Once the functor has started it's atomic and completes despite the timeout.
    let mut ran = false;
    db.write_wait(0.1, |_db| {
        thread::sleep(Duration::from_secs_f64(0.2));
        ran = true;
        Ok(())
    })
    .unwrap();
    assert_eq!(ran, true);

    // Fill the write thread queue with a slow functor
    db.write(0., |_db| thread::sleep(Duration::from_secs_f64(0.2)))
        .unwrap();
    // And now timeout fires before the writer thread gets to the next functor
    let mut ran = false;
    let rc = db.write_wait(0.1, |_db| {
        ran = true;
        Ok(())
    });
    assert!(format!("{:?}", rc).ends_with("Timeout\")"), "{:?}", rc);
    assert_eq!(ran, false)
}

#[bench]
fn write_wait(bm: &mut test::Bencher) {
    let repl = ReplicationArc::from(NopRepl {});
    let db = Arc::new(ReplicatedSQLite::init(":memory:", |_, _| {}, repl).unwrap());
    let start = now_float();
    let mut count = 0;
    bm.iter(|| {
        let cc = db
            .write_wait(9., |db| {
                let cnt = db.count("SELECT 0", &[]).unwrap();
                assert_eq!(cnt, 1);
                count += cnt;
                Ok(count)
            })
            .unwrap();
        assert_eq!(cc, count)
    });
    assert!(count > 0);
    let delta = now_float() - start;
    println!("write_wait: {:.1} o/s", count as f64 / delta)
}
