#[macro_use]
extern crate fomat_macros;
#[macro_use]
extern crate gstuff;
#[macro_use]
extern crate serde_derive;

use crossbeam::channel as chan;
use crossbeam::sync::Unparker;
use crossbeam_utils::sync::Parker;
use futures_timer::Delay;
use gstuff::{any_to_str, now_float, now_ms};
use indexmap::set::IndexSet;
use inlinable_string::InlinableString;
use serde_bytes::ByteBuf;
use smallvec::SmallVec;
use std::any::Any;
use std::borrow::Cow;
use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::fmt;
use std::mem::transmute;
use std::ops::Deref;
use std::os::raw::{c_char, c_int, c_uint, c_void};
use std::panic::{catch_unwind, RefUnwindSafe, UnwindSafe};
use std::pin::Pin;
use std::ptr::{null, null_mut};
use std::slice::from_raw_parts;
use std::str::from_utf8_unchecked;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

#[allow(dead_code, non_snake_case, non_camel_case_types)]
mod sqlite3; // Automatic C bindings.

// The module implements a replicated master-master SQLite pair.
// SQLite databases are embedded, allowing for fast in-process access.
// Write operations are represented by functors, which are batched and executed withing a replicated SQLite session.
// This means that write operations can be complex, fast and nigh unlimited in how they interact with the database.
//
// Why SQLite?
// * Fast in-process database access (no TCP/IP roundtrips when reading or writing, replication happens in background).
// * → Which translates well to some advanced techniques, such as JSON compression or stepping query timeouts.
// * Ease of maintenance (code is more approachable, out-of-sync can be trivially fixed with rsync).
// * → WAL mode is reasonably rsync-friendly, trivial to backup.
// * Decent and extensible full-text search. R*Tree.

// Recommended SQLite build options:
// ./configure --enable-session --enable-readline

// NB: DDL is not a part of a changeset, cf. https://www.mail-archive.com/sqlite-users@mailinglists.sqlite.org/msg103982.html
// NB: It is currently unknown how the transactions affect the session extension. Need to investigate.

// TODO: Switch to normal Rust logging
macro_rules! lo {($($args: tt)+) => {{
  pintln! (
    (::gstuff::filename (file!())) ':' (line!())
    "] " $($args)+);}}}

pub type ChangesetId = i64;
pub type ChangesetCreated = i64;
pub type ChangesetRid = i64;
pub type Changeset = ByteBuf;
#[derive(Clone, Deserialize, Hash, Serialize)]
pub struct ChangesetEn {
    pub id: ChangesetEnId,
    pub changeset: Changeset,
}
#[derive(Serialize, Deserialize, PartialEq, Eq, Hash, Clone, Copy)]
pub struct ChangesetEnId {
    pub id: ChangesetId,
    pub created: ChangesetCreated,
    pub rid: ChangesetRid,
}

fn sqlite3_bind_parameter_index(
    stmt: *mut sqlite3::sqlite3_stmt,
    name: &str,
) -> Result<i32, String> {
    // TODO: Use a stack-allocated buffer instead of CString.
    let c_name = try_s!(CString::new(name));
    let idx = unsafe { sqlite3::sqlite3_bind_parameter_index(stmt, c_name.as_ptr()) };
    if idx == 0 {
        return ERR!("No such parameter: {}", name);
    }
    Ok(idx)
}

// https://www.sqlite.org/c3ref/bind_blob.html
pub trait Parameter {
    /// * `idx` - Parameter index in the parameter array passed to `fn query`. 1-based.
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String>;
}

impl<'a> Parameter for &'a str {
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_text(
                stmt,
                idx,
                self.as_ptr() as *const i8,
                self.len() as i32,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_text: {}", rc);
        }
        Ok(())
    }
}
impl<'a, 'b> Parameter for (&'a str, &'b str) {
    fn bind(&self, _idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_text(
                stmt,
                idx,
                self.1.as_ptr() as *const i8,
                self.1.len() as i32,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_text: {}", rc);
        }
        Ok(())
    }
}
/* Handling `AsRef<str>` instead of `str` should be possible after the https://github.com/rust-lang/rust/issues/45542 is fixed.
`AsRef<str>` will cover `str`, `String`, `InlinableString` and whatever.
Perhaps we should instead define our own trait (see also the traits in nom?) and implement it for str, String, InlinableString,
allowing users to easily add new string-like parameter types. */

impl Parameter for InlinableString {
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_text(
                stmt,
                idx,
                self.as_ptr() as *const i8,
                self.len() as i32,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_text: {}", rc);
        }
        Ok(())
    }
}
impl<'a, 'b> Parameter for (&'a str, &'b InlinableString) {
    fn bind(&self, _idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_text(
                stmt,
                idx,
                self.1.as_ptr() as *const i8,
                self.1.len() as i32,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_text: {}", rc);
        }
        Ok(())
    }
}

impl<'a> Parameter for &'a [u8] {
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_blob64(
                stmt,
                idx,
                self.as_ptr() as *const c_void,
                self.len() as u64,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_blob64: {}", rc);
        }
        Ok(())
    }
}
impl<'a, 'b> Parameter for (&'a str, &'b [u8]) {
    fn bind(&self, _idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        // NB: We don't need the SQLITE_TRANSIENT here because the parameters live till the end of `fn query`.
        let rc = unsafe {
            sqlite3::sqlite3_bind_blob64(
                stmt,
                idx,
                self.1.as_ptr() as *const c_void,
                self.1.len() as u64,
                None,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_blob64: {}", rc);
        }
        Ok(())
    }
}

impl Parameter for i64 {
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let rc = unsafe { sqlite3::sqlite3_bind_int64(stmt, idx, *self) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_int64: {}", rc);
        }
        Ok(())
    }
}
impl<'a> Parameter for (&'a str, i64) {
    fn bind(&self, _idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        let rc = unsafe { sqlite3::sqlite3_bind_int64(stmt, idx, self.1) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_int64 ({}): {}", self.0, rc);
        }
        Ok(())
    }
}

impl Parameter for f64 {
    fn bind(&self, idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let rc = unsafe { sqlite3::sqlite3_bind_double(stmt, idx, *self) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_double: {}", rc);
        }
        Ok(())
    }
}
impl<'a> Parameter for (&'a str, f64) {
    fn bind(&self, _idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        let rc = unsafe { sqlite3::sqlite3_bind_double(stmt, idx, self.1) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3_bind_double ({}): {}", self.0, rc);
        }
        Ok(())
    }
}

impl<'a, T> Parameter for (&'a str, Option<T>)
where
    (&'a str, T): Parameter,
    T: Copy,
{
    fn bind(&self, raw_idx: i32, stmt: *mut sqlite3::sqlite3_stmt) -> Result<(), String> {
        let idx = try_s!(sqlite3_bind_parameter_index(stmt, self.0));
        match self.1 {
            None => {
                let rc = unsafe { sqlite3::sqlite3_bind_null(stmt, idx) };
                if rc != sqlite3::SQLITE_OK as c_int {
                    return ERR!("!sqlite3_bind_null ({}): {}", self.0, rc);
                }
                Ok(())
            }
            Some(v) => (self.0, v).bind(raw_idx, stmt),
        }
    }
}

/// SQLite datatype.  
/// cf. https://sqlite.org/c3ref/c_blob.html
#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum Datatype {
    INTEGER,
    FLOAT,
    BLOB,
    NULL,
    TEXT,
}

#[allow(dead_code)]
impl Datatype {
    pub fn is_integer(&self) -> bool {
        *self == Datatype::INTEGER
    }
    pub fn is_real(&self) -> bool {
        *self == Datatype::FLOAT
    }
    pub fn is_text(&self) -> bool {
        *self == Datatype::TEXT
    }
    pub fn is_blob(&self) -> bool {
        *self == Datatype::BLOB
    }
    pub fn is_null(&self) -> bool {
        *self == Datatype::NULL
    }
}

pub struct Row(*mut sqlite3::sqlite3_stmt, i32);
impl Row {
    /// The number of columns.
    pub fn columns(&self) -> i32 {
        self.1
    }

    /// Invokes `cb` with the name of the given column.
    pub fn column_name(
        &self,
        col: i32,
        cb: &mut dyn FnMut(&str) -> Result<(), String>,
    ) -> Result<(), String> {
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        let cs = unsafe { sqlite3::sqlite3_column_name(self.0, col) };
        if cs == null_mut() {
            return ERR!("!sqlite3_column_name ({})", col);
        }
        let cs = unsafe { CStr::from_ptr(cs) };
        let name = try_s!(cs.to_str());
        try_s!(cb(name));
        Ok(())
    }

    /// Returns the initial data type of the column.  
    /// cf. https://sqlite.org/c3ref/column_blob.html  
    /// `col` is 0-based.
    pub fn datatype(&self, col: i32) -> Result<Datatype, String> {
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        let datatype = match unsafe { sqlite3::sqlite3_column_type(self.0, col) as u32 } {
            sqlite3::SQLITE_INTEGER => Datatype::INTEGER,
            sqlite3::SQLITE_FLOAT => Datatype::FLOAT,
            sqlite3::SQLITE_TEXT => Datatype::TEXT,
            sqlite3::SQLITE_BLOB => Datatype::BLOB,
            sqlite3::SQLITE_NULL => Datatype::NULL,
            x => return ERR!("Unknown datatype code from sqlite3_column_type: {}", x),
        };
        Ok(datatype)
    }

    /// `col` is 0-based.
    pub fn text<'a>(&'a mut self, col: i32) -> Result<&'a str, String> {
        // ^ NB: `mut` protects us from accidentally using an invalidated type conversion buffer.
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        // https://www.sqlite.org/c3ref/column_blob.html
        let pt = unsafe { sqlite3::sqlite3_column_text(self.0, col) };
        let len = unsafe { sqlite3::sqlite3_column_bytes(self.0, col) };
        let slice = unsafe { from_raw_parts(pt, len as usize) };
        Ok(unsafe { from_utf8_unchecked(slice) })
    }

    /// Nullable inlinable string.  
    /// `col` is 0-based.
    pub fn nuinst(&mut self, col: i32) -> Result<Option<InlinableString>, String> {
        if try_s!(self.datatype(col)).is_null() {
            return Ok(None);
        }
        Ok(Some(InlinableString::from(try_s!(self.text(col)))))
    }

    /// Returns the column value as `INTEGER`.  
    /// cf. https://sqlite.org/c3ref/column_blob.html  
    /// `col` is 0-based.
    pub fn integer(&self, col: i32) -> Result<i64, String> {
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        Ok(unsafe { sqlite3::sqlite3_column_int64(self.0, col) })
    }

    /// Returns the column value as `REAL`.  
    /// cf. https://sqlite.org/c3ref/column_blob.html  
    /// `col` is 0-based.
    pub fn real(&self, col: i32) -> Result<f64, String> {
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        Ok(unsafe { sqlite3::sqlite3_column_double(self.0, col) })
    }

    /// `col` is 0-based.
    pub fn blob<'a>(&'a mut self, col: i32) -> Result<&'a [u8], String> {
        // ^ NB: `mut` protects us from accidentally using an invalidated type conversion buffer.
        if col < 0 || col >= self.1 {
            return ERR!("No such column: {}", col);
        }
        let pt = unsafe { sqlite3::sqlite3_column_blob(self.0, col) };
        let len = unsafe { sqlite3::sqlite3_column_bytes(self.0, col) };
        Ok(unsafe { from_raw_parts(pt as *const u8, len as usize) })
    }
}

struct PreparedStatement(*mut sqlite3::sqlite3_stmt);
impl Drop for PreparedStatement {
    fn drop(&mut self) {
        // https://www.sqlite.org/c3ref/finalize.html
        unsafe { sqlite3::sqlite3_finalize(self.0) };
        self.0 = null_mut()
    }
}

/// Interface to `sqlite3_errstr`, see https://sqlite.org/c3ref/errcode.html.
fn code_to_str(error_code: u32) -> &'static str {
    let cs = unsafe { sqlite3::sqlite3_errstr(error_code as c_int) };
    if cs == null() {
        return "!sqlite3_errstr";
    }
    match unsafe { CStr::from_ptr(cs) }.to_str() {
        Ok(cs) => cs,
        Err(_) => "!sqlite3_errstr utf8",
    }
}

/// Wraps the C SQLite handle and provides a few high-level helpers.
pub struct SQLiteHandle {
    pub db: *mut sqlite3::sqlite3,
    static_statements: Mutex<HashMap<usize, PreparedStatement>>,
    timings: fn(&str, u64),
}

impl Drop for SQLiteHandle {
    fn drop(&mut self) {
        if let Ok(mut static_statements) = self.static_statements.lock() {
            static_statements.clear()
        }
        unsafe { sqlite3::sqlite3_close(self.db) };
        self.db = null_mut()
    }
}

impl SQLiteHandle {
    /// Runs the SQL without caching the query
    fn raw_exec(&self, sql_c: *const c_char) -> Result<(), (c_int, Cow<str>)> {
        // https://www.sqlite.org/c3ref/exec.html
        let mut emsg: *mut c_char = null_mut();
        let rc =
            unsafe { sqlite3::sqlite3_exec(self.db, sql_c, None, null_mut(), &mut emsg) } as i32;
        if rc != sqlite3::SQLITE_OK as c_int {
            let emsg = unsafe { CStr::from_ptr(emsg) }.to_string_lossy();
            return Err((rc, emsg));
        }
        Ok(())
    }

    /// Runs the SQL without caching the query
    ///
    /// NB: Values can be escaped safely with the hex encoding (cf. `fn hex_push`):
    ///
    ///     SELECT X'D0BFD180D0B0D0B2D0B4D0B0'; -- правда
    pub fn exec(&self, sql: &str) -> Result<(), String> {
        let sql_c = try_s!(CString::new(sql));
        let started = now_ms();
        let rc = self.raw_exec(sql_c.as_ptr());
        (self.timings)(sql, now_ms() - started);
        if let Err((rc, emsg)) = rc {
            return ERR!("!sqlite3_exec ({}): {}, {}", sql, rc, emsg);
        }
        Ok(())
    }

    fn prepare(&self, sql: &str, persistent: bool) -> Result<PreparedStatement, String> {
        let sql = try_s!(CString::new(sql));
        let mut stmt: *mut sqlite3::sqlite3_stmt = null_mut();
        // https://www.sqlite.org/c3ref/prepare.html
        let rc = unsafe {
            sqlite3::sqlite3_prepare_v3(
                self.db,
                sql.as_ptr(),
                sql.as_bytes().len() as i32,
                if persistent {
                    sqlite3::SQLITE_PREPARE_PERSISTENT
                } else {
                    0
                },
                &mut stmt,
                null_mut(),
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            // Memory-safe but not thread-safe (there's a chance we'll get a different error message because of a race).
            // TODO: Consider taking a mutex, cf. https://sqlite.org/c3ref/errcode.html
            let emsg = unsafe { CStr::from_ptr(sqlite3::sqlite3_errmsg(self.db)) }
                .to_string_lossy()
                .into_owned();
            return ERR!("!sqlite3_prepare_v3: {}, {}", rc, emsg);
        }
        Ok(PreparedStatement(stmt))
    }

    /// Prepares and caches the query, binds the parameters, invokes `cb` while stepping through the results.
    ///
    /// Stepping through the rows stops if `cb` returns `false`.
    ///
    /// Other queries can be run recursively from the callback.
    ///
    /// Example using the `query` with a `params` variable:
    ///
    ///     let mut sum = None;
    ///     let params: &[&dyn sqlite_mm::Parameter] = &[&2, &3];
    ///     try_s! (sqlite.db.query ("SELECT ? + ?", params, &mut |row| {
    ///       sum = Some (try_s! (row.integer (0)));
    ///       Ok (false)
    ///     }));
    ///     assert_eq! (sum, Some (5));
    ///
    /// Example using the `query` with inline params:
    ///
    ///     let mut sum = None;
    ///     try_s! (sqlite.db.query ("SELECT ? + ?", &[&2, &3], &mut |row| {sum = Some (try_s! (row.integer (0))); Ok (false)}));
    ///     assert_eq! (sum, Some (5));
    pub fn query(
        &self,
        sql: &'static str,
        params: &[&dyn Parameter],
        cb: &mut dyn FnMut(Row) -> Result<bool, String>,
    ) -> Result<(), String> {
        let started = now_ms();
        let sql_addr = sql.as_ptr() as usize;
        let stmt = {
            let mut static_statements = try_s!(self.static_statements.lock());
            // NB: *Moving* the cached prepared statement out from the cache allows us to:
            //     1) Process the same query in parallel, by preparing a new statement for the extra threads;
            //     2) Avoid `Mutex` poisoning whenever `cb` panics, by not holding to a statement lock.
            if let Some(cached) = static_statements.remove(&sql_addr) {
                cached
            } else {
                try_s!(self.prepare(sql, true))
            }
        };

        let k = (|| -> Result<(), String> {
            for (parameter, idx) in params.iter().zip(1..) {
                try_s!(parameter.bind(idx, stmt.0))
            }

            let columns = unsafe { sqlite3::sqlite3_column_count(stmt.0) };

            loop {
                // https://www.sqlite.org/c3ref/step.html
                match unsafe { sqlite3::sqlite3_step(stmt.0) } as u32 {
                    sqlite3::SQLITE_DONE => break,
                    sqlite3::SQLITE_ROW => {
                        if !try_s!(cb(Row(stmt.0, columns))) {
                            break;
                        }
                    }
                    rc => return ERR!("!sqlite3_step, code: {} ({})", rc, code_to_str(rc)),
                }
            }

            Ok(())
        })();

        (self.timings)(sql, now_ms() - started);

        // (Re)cache the prepared statement.

        // https://www.sqlite.org/c3ref/reset.html
        // NB: This will also release any locks still held by the query.
        unsafe { sqlite3::sqlite3_reset(stmt.0) };
        // https://www.sqlite.org/c3ref/clear_bindings.html
        unsafe { sqlite3::sqlite3_clear_bindings(stmt.0) };

        let mut static_statements = try_s!(self.static_statements.lock());
        static_statements.insert(sql_addr, stmt);

        k
    }

    /// Same as `fn query` but doesn't cache the prepared statement.
    ///
    /// Use with caution: dynamically generated SQL strings might be subject to SQL injection vulnerabilities.
    pub fn query_once(
        &self,
        sql: &str,
        params: &[&dyn Parameter],
        cb: &mut dyn FnMut(Row) -> Result<bool, String>,
    ) -> Result<(), String> {
        let started = now_ms();

        let stmt = try_s!(self.prepare(sql, false));

        let k = (|| -> Result<(), String> {
            for (parameter, idx) in params.iter().zip(1..) {
                try_s!(parameter.bind(idx, stmt.0))
            }

            let columns = unsafe { sqlite3::sqlite3_column_count(stmt.0) };

            loop {
                // https://www.sqlite.org/c3ref/step.html
                match unsafe { sqlite3::sqlite3_step(stmt.0) } as u32 {
                    sqlite3::SQLITE_DONE => break,
                    sqlite3::SQLITE_ROW => {
                        if !try_s!(cb(Row(stmt.0, columns))) {
                            break;
                        }
                    }
                    rc => return ERR!("!sqlite3_step, code: {} ({})", rc, code_to_str(rc)),
                }
            }

            Ok(())
        })();

        (self.timings)(sql, now_ms() - started);

        k
    }

    /// Calls the [query] and counts the number of rows returned.
    pub fn count(&self, sql: &'static str, params: &[&dyn Parameter]) -> Result<u32, String> {
        let mut count = 0;
        try_s!(self.query(sql, params, &mut |_| {
            count += 1;
            Ok(true)
        }));
        Ok(count)
    }

    /// Prepares and caches the query, binds the parameters, invokes the database.
    ///
    /// Example of an upsert:
    ///
    ///     let params: &[&dyn sqlite_mm::Parameter] = &[&("$bar", 123 as i64)];
    ///     try_s! (db.dml ("UPDATE foo SET bar = $bar", params));
    ///     if db.changes() == 0 {try_s! (db.dml ("INSERT INTO foo (bar) VALUES ($bar)", params))}
    ///
    /// (See also https://sqlite.org/lang_UPSERT.html).
    ///
    /// Insert:
    ///
    ///     try_s! (db.dml ("INSERT INTO foo (bar) VALUES ($bar)", &[&("$bar", bar)]));
    ///
    /// Update with positional question mark placeholders (cf. https://sqlite.org/lang_expr.html#varparam):
    ///
    ///     let rc = db.dml (
    ///       "UPDATE disco_rollz SET last_used = ? WHERE name = ? AND owner_id = ? AND server_id = ?",
    ///       &[&now, &name, &owner_id, &server_id]
    ///     );
    ///     if let Err (err) = rc {lo! ("dice_io] Error updating disco_rollz/last_used: " (err))}}))}
    ///
    /// If an insert (or an SQLite upsert targeting a different conflict) fails
    /// due to uniqueness constraint violation then the returned error ends with
    /// "!sqlite3_step, code: 19 (constraint failed)".
    pub fn dml(&self, sql: &'static str, params: &[&dyn Parameter]) -> Result<(), String> {
        self.query(sql, params, &mut |_| Ok(false))
    }

    /// http://sqlite.org/c3ref/changes.html
    ///
    /// NB: For this function to work properly, all INSERT, UPDATE and DELETE statements should happen on the writer thread,
    /// because otherwise a parallel DML query might affect the number of modified rows.
    /// ("If a separate thread makes changes on the same database connection while sqlite3_changes() is running
    ///   then the value returned is unpredictable and not meaningful.")
    pub fn changes(&self) -> u32 {
        // The function result should be treated as an unsigned integer probably:
        // http://sqlite.1065341.n5.nabble.com/Possible-Bug-Return-type-on-change-functions-td59659.html
        unsafe { sqlite3::sqlite3_changes(self.db) as u32 }
    }

    /// `true` if we're not inside a transaction  
    /// (and consequently `false` if we are in transaction)
    pub fn get_autocommit(&self) -> bool {
        // https://www.sqlite.org/c3ref/get_autocommit.html
        unsafe { sqlite3::sqlite3_get_autocommit(self.db) != 0 }
    }
}

/// Marks `Arc<SQLiteHandle>` thread-safe.
#[derive(Clone)]
pub struct ArcSQLiteHandle(Arc<SQLiteHandle>);
unsafe impl Send for ArcSQLiteHandle {}
unsafe impl Sync for ArcSQLiteHandle {}
impl Deref for ArcSQLiteHandle {
    type Target = SQLiteHandle;
    fn deref(&self) -> &SQLiteHandle {
        &*self.0
    }
}

pub fn hex_from_digit(num: u8) -> char {
    if num < 10 {
        (b'0' + num) as char
    } else {
        (b'A' + num - 10) as char
    }
}

// benchmark: https://stackoverflow.com/a/48485777/257568.
#[allow(dead_code)]
pub fn hex_push(buf: &mut String, blob: &[u8]) {
    for ch in blob {
        buf.push(hex_from_digit(ch / 16));
        buf.push(hex_from_digit(ch % 16))
    }
}

/// Run a scoped `cb` on the writer thread
struct WriteWait {
    /// `true` if the scope still exists
    alive: bool,
    /// Callback that is only valid while the calling scope is `alive`
    cb: *mut (dyn FnMut(ArcSQLiteHandle) + Send + RefUnwindSafe),
    /// Result of `catch_unwind` on `cb`
    cr: Option<Result<(), Box<(dyn Any + Send + 'static)>>>,
    /// Wakes up the callsite
    unparker: Unparker,
}

enum ReplicatedWrite {
    Background(Box<dyn FnOnce(ArcSQLiteHandle) + Send + UnwindSafe>),
    Scoped(Arc<Mutex<WriteWait>>),
}
unsafe impl Send for ReplicatedWrite {}

enum WriteOp {
    ReplicatedWrite(ReplicatedWrite),
    IncomingChangesets(Vec<ChangesetEn>, chan::Sender<()>),
    RemoveSyncedChangesets(Vec<ChangesetEnId>),
}

struct SQLiteSession(Arc<SQLiteHandle>, *mut sqlite3::sqlite3_session);
impl SQLiteSession {
    fn create(db: Arc<SQLiteHandle>) -> Result<SQLiteSession, String> {
        let mut p_session: *mut sqlite3::sqlite3_session = null_mut();
        let rc = unsafe {
            sqlite3::sqlite3session_create(db.db, b"main\0".as_ptr() as *const i8, &mut p_session)
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3session_create: {}", rc);
        }
        Ok(SQLiteSession(db, p_session))
    }
    fn attach(&mut self) -> Result<(), String> {
        let rc = unsafe { sqlite3::sqlite3session_attach(self.1, null_mut()) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3session_attach: {}", rc);
        }
        Ok(())
    }
    /// https://sqlite.org/session/sqlite3session_enable.html
    // fn enable (&mut self) {
    //   let rc = unsafe {sqlite3::sqlite3session_enable (self.1, 1)};
    //   assert_eq! (rc, 1)}
    fn disable(&mut self) {
        let rc = unsafe { sqlite3::sqlite3session_enable(self.1, 0) };
        assert_eq!(rc, 0)
    }
    fn changeset(&mut self) -> Result<Vec<u8>, String> {
        let mut size: c_int = 0;
        let mut buf: *mut c_void = null_mut();
        // https://sqlite.org/session/sqlite3session_changeset.html
        let rc = unsafe { sqlite3::sqlite3session_changeset(self.1, &mut size, &mut buf) };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3session_changeset: {}", rc);
        }
        let v = unsafe { from_raw_parts(buf as *const u8, size as usize) }.to_vec();
        // https://sqlite.org/c3ref/free.html
        unsafe { sqlite3::sqlite3_free(buf) };
        Ok(v)
    }
}
impl Drop for SQLiteSession {
    fn drop(&mut self) {
        assert!(self.1 != null_mut());
        unsafe { sqlite3::sqlite3session_delete(self.1) };
        self.1 = null_mut()
    }
}

// https://sqlite.org/session/sqlite3changeset_apply.html
extern "C" fn conflict(
    _ctx: *mut c_void,
    conflict: c_int,
    _iter: *mut sqlite3::sqlite3_changeset_iter,
) -> c_int {
    //lo! ("conflict] conflict: " (conflict));
    if conflict == sqlite3::SQLITE_CHANGESET_DATA as c_int
        || conflict == sqlite3::SQLITE_CHANGESET_CONFLICT as c_int
    {
        sqlite3::SQLITE_CHANGESET_REPLACE as c_int
    } else {
        sqlite3::SQLITE_CHANGESET_OMIT as c_int
    }
}

/// Iterates through the given changeset to see what tables were changed.  
/// Useful for implementing the table modification callbacks.
fn explore_changeset(changeset: &[u8]) -> Result<(), String> {
    // https://sqlite.org/session/sqlite3changeset_start.html
    let mut it: *mut sqlite3::sqlite3_changeset_iter = null_mut();
    let rc = unsafe {
        sqlite3::sqlite3changeset_start(
            &mut it,
            changeset.len() as c_int,
            changeset.as_ptr() as *mut c_void,
        )
    };
    if rc != sqlite3::SQLITE_OK as c_int {
        return ERR!("!sqlite3changeset_start: {}", rc);
    }

    // https://sqlite.org/session/sqlite3changeset_finalize.html
    struct It(*mut sqlite3::sqlite3_changeset_iter);
    impl Drop for It {
        fn drop(&mut self) {
            let _rc = unsafe { sqlite3::sqlite3changeset_finalize(self.0) };
        }
    }
    let it = It(it);

    // https://sqlite.org/session/sqlite3changeset_next.html
    loop {
        let rc = unsafe { sqlite3::sqlite3changeset_next(it.0) };
        if rc == sqlite3::SQLITE_DONE as c_int {
            break;
        }
        if rc != sqlite3::SQLITE_ROW as c_int {
            return ERR!("!sqlite3changeset_next: {}", rc);
        }

        // https://sqlite.org/session/sqlite3changeset_op.html
        let mut table_name: *const c_char = null();
        let mut columns = 0;
        let mut op = 0;
        let mut indirect = 0;
        let rc = unsafe {
            sqlite3::sqlite3changeset_op(
                it.0,
                &mut table_name,
                &mut columns,
                &mut op,
                &mut indirect,
            )
        };
        if rc != sqlite3::SQLITE_OK as c_int {
            return ERR!("!sqlite3changeset_op: {}", rc);
        }

        // TODO: The callback should be able to utilize https://sqlite.org/session/sqlite3changeset_new.html
        //       (and https://sqlite.org/session/sqlite3changeset_old.html).

        if table_name == null() {
            return ERR!("table_name is null");
        }
        let table_name = unsafe { CStr::from_ptr(table_name) };
        let _table_name = try_s!(table_name.to_str());
        //lo! ("explore_changeset] " [=table_name]);
    }

    Ok(())
}

/// Try to lock the database
///
/// We can think of SQLite support for concurrency as low level / non-blocking.
/// In Rust terms it gives us `try_lock` instead of the usual `lock`.
/// This has a benefit of allowing us to pick the intensity of the spinning
/// and to timeout on deadlocks instead of just hanging indefinitely.
/// Locks that can timeout are preferable to locks that just hang
/// ( a design we've also explored in
/// https://github.com/KomodoPlatform/atomicDEX-API/blob/ce65f31e7663513895e833c6fabe660ac6c927da/mm2src/common/duplex_mutex.rs ).
fn begin_immediate(db: &SQLiteHandle) -> Result<(), String> {
    let begin = now_float();
    loop {
        let sql_c = b"BEGIN IMMEDIATE\0";
        let rc = db.raw_exec(sql_c.as_ptr() as *const c_char);
        if let Err((rc, emsg)) = rc {
            if rc == sqlite3::SQLITE_BUSY as c_int || rc == sqlite3::SQLITE_LOCKED as c_int {
                let delta = now_float() - begin;
                if delta < 3.141 {
                    // Wait for the locking transaction to go away on its own
                    thread::sleep(Duration::from_millis(22));
                    continue;
                }
                if delta < 7.77 {
                    // Complain, for a chance of finding a cause in the adjucent logs
                    let ac = db.get_autocommit();
                    lo! ("Waiting " {"{:1}", delta} "s for DB to unlock; autocommit is " (ac));
                    thread::sleep(Duration::from_secs_f64(delta / 3.14));
                    continue;
                }
                if delta < 9.99 && !db.get_autocommit() {
                    // Try to rollback a prior transaction
                    // (maybe another thread has finished prematurely and could not close it)
                    thread::sleep(Duration::from_millis(22));
                    let sql_c = b"ROLLBACK\0";
                    let _ = db.raw_exec(sql_c.as_ptr() as *const c_char);
                    continue;
                }
                return ERR!("!BEGIN IMMEDIATE after {:.1}s: {}, {}", delta, rc, emsg);
            } else {
                return ERR!("!BEGIN IMMEDIATE: {}, {}", rc, emsg);
            }
        } else {
            return Ok(());
        }
    }
}

fn writer_thread(
    db: ArcSQLiteHandle,
    writer_rx: chan::Receiver<WriteOp>,
    replicator_tx: Option<chan::Sender<()>>,
) -> Result<(), String> {
    loop {
        // Get a batch of operations from the channel.

        let mut replicated_ops = SmallVec::<[ReplicatedWrite; 16]>::new();
        let mut incoming_changesets = SmallVec::<[(Vec<ChangesetEn>, chan::Sender<()>); 1]>::new();
        let mut synchronized_changesets = SmallVec::<[Vec<ChangesetEnId>; 1]>::new();
        macro_rules! triage {
            ($op: expr) => {
                match $op {
                    WriteOp::ReplicatedWrite(rw) => replicated_ops.push(rw),
                    WriteOp::IncomingChangesets(changesets, tx) => {
                        incoming_changesets.push((changesets, tx))
                    }
                    WriteOp::RemoveSyncedChangesets(changeset_ids) => {
                        synchronized_changesets.push(changeset_ids)
                    }
                }
            };
        }
        if let Ok(op) = writer_rx.recv() {
            triage!(op)
        } else {
            return Ok(());
        }
        loop {
            if let Ok(op) = writer_rx.try_recv() {
                triage!(op)
            } else {
                break;
            }
        }

        if !incoming_changesets.is_empty() || !synchronized_changesets.is_empty() {
            // Modify changesets inside a single transaction in order to hopefully speed things up a bit.
            // NB: `IMMEDIATE` ensures a single writer, cf. https://sqlite.org/lang_transaction.html
            try_s!(begin_immediate(&db));

            // Merge the incoming changesets.

            for (changesets, tx) in incoming_changesets {
                //lo! ("writer_thread] Got " (changesets.len()) " changeset(s) from the peer.");
                for en in changesets {
                    //lo! ("writer_thread] Merging changeset " (en.id.id) ", " (en.changeset.len()) " bytes long ...");
                    // https://sqlite.org/session/sqlite3changeset_apply.html
                    let rc = unsafe {
                        sqlite3::sqlite3changeset_apply(
                            db.db,
                            en.changeset.len() as c_int,
                            en.changeset.as_ptr() as *mut c_void,
                            None,
                            Some(conflict),
                            null_mut(),
                        )
                    };
                    if rc != sqlite3::SQLITE_OK as c_int {
                        let desc = match rc as c_uint {
                            // I've seen this one happen when SQLite was denied access to "/var/tmp/etilqs_37d03962b261c42c".
                            sqlite3::SQLITE_CANTOPEN => "SQLITE_CANTOPEN",
                            // "If the conflict-handler returns an illegal value,
                            // any changes already made are rolled back and the call to sqlite3changeset_apply() returns SQLITE_MISUSE."
                            sqlite3::SQLITE_MISUSE => "SQLITE_MISUSE",
                            _ => "",
                        };
                        return ERR!("sqlite3changeset_apply: {} ({})", rc, desc);
                    }
                    // Fire table callbacks.
                    if let Err(err) = explore_changeset(&en.changeset) {
                        lo!("!explore_changeset: "(err))
                    }
                }
                // Notify the replication server that we've done with this chunk of changesets.
                if let Err(_v) = tx.try_send(()) {
                    lo!("!send")
                }
            }

            // Remove changesets that were synchronized.

            for changeset_ids in synchronized_changesets {
                for ChangesetEnId { id, created, rid } in changeset_ids {
                    //lo! ("writer_thread] Removing a synchronized changeset " (id) " ...");
                    if let Err(err) = db.dml(
                        "DELETE FROM _changesets_ WHERE id = ? AND created = ? AND rid = ?",
                        &[&id, &created, &rid],
                    ) {
                        return ERR!("!DELETE: {}", err);
                    }
                }
            }

            if let Err(err) = db.exec("COMMIT") {
                return ERR!("writer_thread] !COMMIT: {}", err);
            }
        }

        // Execute the write callbacks inside a replicable session.

        if !replicated_ops.is_empty() {
            // Run the write operations inside a transaction.
            // NB: `IMMEDIATE` ensures a single writer, cf. https://sqlite.org/lang_transaction.html
            try_s!(begin_immediate(&db));
            // Open session.
            let mut session = match SQLiteSession::create(db.0.clone()) {
                Ok(s) => s,
                Err(err) => return ERR!("writer_thread] {}", err),
            };
            if let Err(err) = session.attach() {
                return ERR!("!create: {}", err);
            }
            for rw in replicated_ops {
                match rw {
                    ReplicatedWrite::Background(bg) => {
                        let cr = {
                            let db = db.clone();
                            catch_unwind(move || -> Result<(), String> {
                                bg(db);
                                Ok(())
                            })
                        };
                        match cr {
                            Ok(Ok(())) => (),
                            Ok(Err(err)) => lo!("writer_thread op error: "(err)),
                            Err(err) => lo!("writer_thread op panic: "[any_to_str(&*err)]),
                        }
                    }
                    ReplicatedWrite::Scoped(arc) => {
                        let mut write_wait = try_s!(arc.lock());
                        if write_wait.alive {
                            let cnt = Arc::strong_count(&arc);
                            if cnt != 2 {
                                return ERR!("Unexpected strong_count: {}", cnt);
                            }
                            let cb = write_wait.cb;
                            write_wait.cr = Some(catch_unwind(|| {
                                let cb: &mut (dyn FnMut(ArcSQLiteHandle) + Send) =
                                    unsafe { &mut *cb };
                                cb(db.clone())
                            }));
                            write_wait.unparker.unpark()
                        }
                    }
                };
                if db.get_autocommit() {
                    return ERR!("Unexpected loss of transaction");
                }
            }
            let changeset = match session.changeset() {
                Ok(cs) => cs,
                Err(err) => return ERR!("!changeset: {}", err),
            };
            session.disable();
            // Save the changeset to the database.
            // COMMIT will persist both the changes and the changeset, making the replication more reliable.
            if !changeset.is_empty() {
                let insert = "INSERT INTO _changesets_ (changeset, created, rid) VALUES ($changeset, $created, random())";
                if let Err(err) = db.dml(
                    insert,
                    &[
                        &("$changeset", &changeset[..]),
                        &("$created", now_ms() as i64),
                    ],
                ) {
                    lo! ("writer_thread] Can't save the changeset! " (err) '\n' (insert));
                    continue;
                }
            }
            if let Err(err) = db.exec("COMMIT") {
                lo!("writer_thread] !COMMIT: "(err));
                continue;
            }
            if !changeset.is_empty() {
                if let Err(err) = explore_changeset(&changeset) {
                    lo!("!explore_changeset: "(err))
                }
                if let Some(replicator_tx) = replicator_tx.as_ref() {
                    // NB: It's okay if replicator is busy with previous sessions
                    let _ = replicator_tx.try_send(());
                }
            }
        }
    }
}

/// Plugin responsible for delivering the changesets to the peers.
pub trait Replication {
    // Passing a `Vec` because it might be easier to serialize with Serde comparing to a slice.
    /// This method gets a number of changesets that weren't synchronized yet.
    /// It can then send the changesets to the SQLite replication peers.
    /// There might be situations when the method might want to ignore the changesets
    /// (bandwidth shaping, delegating replication to other process, etc).
    /// The method, being invoked from a dedicated replication system thread, might currently block
    /// (it can wait for the peer server to confirm the replication, for example).
    /// Changesets whose IDs were returned by the method are marked as synchronized.
    fn publish(&self, changesets: &Vec<ChangesetEn>) -> Result<Vec<ChangesetEnId>, String>;
}

/// Thread-safe Replication.
#[derive(Clone)]
pub struct ReplicationArc(pub Pin<Arc<dyn Replication + Send + Sync + 'static>>);
impl Deref for ReplicationArc {
    type Target = dyn Replication + 'static;
    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}
impl From<Pin<Arc<dyn Replication + Send + Sync + 'static>>> for ReplicationArc {
    fn from(arc: Pin<Arc<dyn Replication + Send + Sync + 'static>>) -> ReplicationArc {
        ReplicationArc(arc)
    }
}
impl<R: Replication + Send + Sync + 'static> From<R> for ReplicationArc {
    fn from(replication: R) -> ReplicationArc {
        ReplicationArc(Arc::pin(replication))
    }
}

fn replicator_thread(
    db: ArcSQLiteHandle,
    replicator_rx: chan::Receiver<()>,
    writer_tx: chan::Sender<WriteOp>,
    replication: ReplicationArc,
) {
    let mut synchronized: IndexSet<ChangesetEnId> = IndexSet::new();

    loop {
        let _ = match replicator_rx.recv_timeout(Duration::from_secs(2)) {
            Ok(_) => (),
            Err(chan::RecvTimeoutError::Timeout) => (),
            Err(chan::RecvTimeoutError::Disconnected) => break,
        };

        // Load the changesets from the database, in order.

        // The ROWID generation is random after we've got a 9223372036854775807 (2^63-1) id.
        // But the 9223372036854775807 will be removed eventually and we'll get a normal changeset ordering again.
        // Thus the window of the random changeset ordering will be small and we can probably accept its presence for now.
        // In the future the ordering guarantee can be provided by looping the IDs:
        // 1) We mentally split the IDs into the low, middle and high zones.
        // 2) If 9223372036854775807 is reached, IDs are generated sequentially in the low zone: SELECT MAX (id) + 1 FROM t WHERE id < 2147483647.
        // 3) If there are high zone IDs present, then we process them before the low and middle zones.

        let mut changesets: Vec<ChangesetEn> = Vec::new();
        let k = db.query(
            "SELECT id, changeset, created, rid FROM _changesets_ ORDER BY id",
            &[],
            &mut |mut row| {
                let id = try_s!(row.integer(0));
                let created = try_s!(row.integer(2));
                let rid = try_s!(row.integer(3));

                let id = ChangesetEnId { id, created, rid };

                if synchronized.contains(&id) {
                    //lo! ("replicator_thread] Changeset " (id) " was already synchronized earlier, skipping...");
                } else {
                    let changeset = try_s!(row.blob(1));
                    let changeset = ByteBuf::from(changeset);
                    //lo! ("replicator_thread] Found a changeset, id " (id) ", " (changeset.len()) " bytes.");
                    changesets.push(ChangesetEn { id, changeset })
                }
                Ok(true)
            },
        );
        if let Err(err) = k {
            lo!("replicator_thread] Error getting the changesets: "(err));
            continue;
        }

        if changesets.is_empty() {
            continue;
        }

        // Send a batch of changesets to the peer.

        let synchronized_ids = match replication.publish(&changesets) {
            Ok(v) => v,
            Err(err) => {
                lo!("replicator_thread] "(err));
                continue;
            }
        };

        if synchronized_ids.is_empty() {
            continue;
        }

        // Synchronized changesets are removed *later* by the writer thread, so we want to keep the ids in the `synchronized` index for a while,
        // but we also want to keep a lid on the size of that index.

        let created_max = changesets.iter().map(|en| en.id.created).max();
        if let Some(created_max) = created_max {
            synchronized.retain(|id| created_max - id.created < 60 * 1000)
        }

        // Remember the synchronized changesets in order not to resync them while the writer thread has not yet removed them from the database.

        for &tuple in &synchronized_ids {
            synchronized.insert(tuple);
        }

        // Tell the writer thread to remove the ACKed changesets (outside of a session).

        if let Err(err) = writer_tx.try_send(WriteOp::RemoveSyncedChangesets(synchronized_ids)) {
            lo!("replicator_thread] !writer_tx.try_send: "(err))
        }
    }
}

/// Replicated SQLite instance.  
/// Owns the writing and replicating threads.
pub struct ReplicatedSQLite {
    /// SQLite database location.
    pub path: &'static str,
    pub replication: ReplicationArc,
    pub db: ArcSQLiteHandle,
    writer_tx: chan::Sender<WriteOp>,
}

impl Drop for ReplicatedSQLite {
    // NB: Expecting the writer and replicator threads to stop when their channels are closed.
    fn drop(&mut self) {}
}

/// A token Debug implementation in order to allow Debug derives in user code.
impl fmt::Debug for ReplicatedSQLite {
    fn fmt(&self, ft: &mut fmt::Formatter<'_>) -> fmt::Result {
        wite!(ft, "ReplicatedSQLite")
    }
}

impl ReplicatedSQLite {
    /// * `path` - SQLite database location. ":memory:" for a temporary in-memory database.
    /// * `timings` - Callback receiving the query timings (which should be more convenient than manually timing each query).
    /// * `replication` - Plugin responsible for delivering the changesets to the peers.
    pub fn init(
        path: &'static str,
        timings: fn(&str, u64),
        replication: ReplicationArc,
    ) -> Result<ReplicatedSQLite, String> {
        let mut db: *mut sqlite3::sqlite3 = null_mut();
        let path_c = try_s!(CString::new(path));
        // https://sqlite.org/c3ref/open.html
        let rc = unsafe {
            sqlite3::sqlite3_open_v2(
                path_c.as_ptr() as *const c_char,
                &mut db,
                // https://www.sqlite.org/threadsafe.html
                (sqlite3::SQLITE_OPEN_READWRITE
                    | sqlite3::SQLITE_OPEN_CREATE
                    | sqlite3::SQLITE_OPEN_FULLMUTEX) as i32,
                null_mut(),
            )
        };
        if rc != 0 {
            let emsg = unsafe { CStr::from_ptr(sqlite3::sqlite3_errmsg(db)) }
                .to_string_lossy()
                .into_owned();
            unsafe { sqlite3::sqlite3_close(db) };
            return ERR!("!sqlite3_open ({}): {}, {}", path, rc, emsg);
        }

        let static_statements = Mutex::new(HashMap::new());
        let db = ArcSQLiteHandle(Arc::new(SQLiteHandle {
            db,
            static_statements,
            timings,
        }));

        // https://www.sqlite.org/pragma.html#pragma_page_size
        try_s!(db.exec("PRAGMA page_size = 32768"));

        // https://www.sqlite.org/wal.html
        try_s!(db.exec("PRAGMA journal_mode = WAL"));
        try_s!(db.exec("PRAGMA synchronous = NORMAL"));

        // https://sqlite.org/foreignkeys.html#fk_enable
        try_s!(db.exec("PRAGMA foreign_keys = ON"));

        // The time (in milliseconds) we wait for a lock before giving up with SQLITE_BUSY.
        // NB: As of version 3310100 this isn't super reliable:
        // `BEGIN IMMEDIATE` might return `SQLITE_BUSY` after roughly the `busy_timeout`
        // yet a manual retry in just the `22` ms is working just fine
        // (or it might return `SQLITE_BUSY` immediately and again a retry works fine);
        // so we're keeping the `busy_timeout` low
        // in order to give us more leeway with manual retries;
        // might eventually switch to just the manual retries,
        // but might need to implement the query retries before that.
        try_s!(db.exec("PRAGMA busy_timeout = 314"));

        // Create a table to store the changesets.
        try_s! (db.exec ("CREATE TABLE IF NOT EXISTS _changesets_ (
      id INTEGER PRIMARY KEY NOT NULL,  -- The ordered id, allowing us to replay the changes in the same order.
      changeset BLOB NOT NULL,  -- Changes captured by an SQLite session and queued for replication to a peer.
      created INTEGER NOT NULL,  -- Entry creation time, in UTC milliseconds minus 1518220765242.
      rid INTEGER NOT NULL  -- Second ID, allowing us to recognize one changeset from another when the `id` is reused.
    )"));

        let (writer_tx, writer_rx) = chan::bounded::<WriteOp>(256);

        let (replicator_tx, replicator_rx) = chan::bounded::<()>(16);
        {
            let db = db.clone();
            try_s!(thread::Builder::new()
                .name("sqlite-writer".into())
                .spawn(move || {
                    let rc = writer_thread(db, writer_rx, Some(replicator_tx));
                    if let Err(err) = rc {
                        lo!("!writer_thread: "(err));
                        panic!("!writer_thread: {}", err)
                    }
                }));
        }

        {
            let db = db.clone();
            let writer_tx = writer_tx.clone();
            let replication = replication.clone();
            try_s!(thread::Builder::new()
                .name("sqlite-replicator".into())
                .spawn(move || replicator_thread(db, replicator_rx, writer_tx, replication)));
        }

        Ok(ReplicatedSQLite {
            path,
            replication,
            db,
            writer_tx,
        })
    }

    /// Receiving end of replication.
    pub async fn changesets_handler(&self, changesets: Vec<ChangesetEn>) -> Result<(), String> {
        // Send the changesets to the writer thread.
        let (tx, rx) = chan::bounded::<()>(1);
        let mut package = WriteOp::IncomingChangesets(changesets, tx);
        loop {
            match self.writer_tx.try_send(package) {
                Ok(()) => break,
                Err(chan::TrySendError::Full(v)) => {
                    //lo! ("changesets_handler] Write queue full, waiting…");
                    package = v;
                    Delay::new(Duration::from_secs_f32(0.1)).await
                }
                Err(chan::TrySendError::Disconnected(_)) => return ERR!("!writer_tx"),
            }
        }

        // Wait for the chunk of changesets to be merged.
        loop {
            match rx.try_recv() {
                Ok(()) => break,
                Err(chan::TryRecvError::Empty) => Delay::new(Duration::from_secs_f32(0.1)).await,
                Err(chan::TryRecvError::Disconnected) => return ERR!("writer_thread disconnected"),
            }
        }
        Ok(())
    }

    /// Schedule the callback to be executed inside the write thread.  
    /// The callback will run inside a batching transaction.  
    /// Database modifications will be saved with a session and forwarded to the peer replica.  
    /// Might return an error if the write queue is full.
    ///
    /// The callback should be limited to modifying the database, it should not be doing something else on the writer thread
    /// (in order not to delay the writer thread unnecessarily and in order to stick to a simpler and less error-prone code flow).
    ///
    /// The callback should not affect the batching transaction (it should not be calling BEGIN, COMMIT or ROLLBACK)
    /// but it PROBABLY can use the "https://www.sqlite.org/lang_savepoint.html" nested transactions.
    /// (TODO: Investigate how transactions and session extension interoperate).
    ///
    /// DDL changes are not replicated, only the DML changes are.
    /// But it's still a good idea to perform the DDL operations on the write thread,
    /// for otherwise the write thread transactions might interfere with the DDL modifications.
    ///
    /// If there is a `timeout` and the writer thread channel is full
    /// then we're try to resend the operation until the `timeout` ends.
    ///
    /// Example:
    ///
    ///     sqlite.write (|db| {
    ///       try_s! (db.exec ("CREATE TABLE IF NOT EXISTS test (i INTEGER PRIMARY KEY NOT NULL) WITHOUT ROWID"));
    ///       try_s! (db.exec ("INSERT INTO test (i) VALUES (strftime ('%s', 'now'))"));
    ///       Ok(())
    ///     });
    ///
    /// Upsert:
    ///
    ///     sqlite.write (move |db| {
    ///       let parameters: &[&dyn sqlite_mm::Parameter] = &[
    ///         &("$foo", 1i64),
    ///         &("$bar", 2i64)
    ///       ];
    ///       let update = "UPDATE foobar SET foo = $foo WHERE bar = $bar";
    ///       let insert = "INSERT INTO foobar (foo, bar) VALUES ($foo, $bar)";
    ///       if let Err (err) = db.dml (update, parameters) {lo! ("Error updating foobar: " (err)); return}
    ///       if db.changes() == 0 {
    ///         if let Err (err) = db.dml (insert, parameters) {lo! ("Error inserting into foobar: " (err)); return}
    ///       }
    ///     });
    ///
    /// (See also https://sqlite.org/lang_UPSERT.html).
    pub fn write<F>(&self, timeout: f64, cb: F) -> Result<(), String>
    where
        F: 'static + FnOnce(ArcSQLiteHandle) + Send + UnwindSafe,
    {
        let mut write_op = WriteOp::ReplicatedWrite(ReplicatedWrite::Background(Box::new(cb)));
        let mut started = std::f64::NAN;
        loop {
            match self.writer_tx.try_send(write_op) {
                Ok(()) => break Ok(()),
                Err(chan::TrySendError::Disconnected(_)) => {
                    break ERR!("Writer thread disconnected")
                }
                Err(chan::TrySendError::Full(op)) => {
                    write_op = op;
                    let now = now_float();
                    if timeout.is_nan() || timeout <= 0. {
                        break ERR!("Timeout");
                    } else if started.is_nan() {
                        started = now
                    } else if now - started > timeout {
                        break ERR!("Timeout");
                    }
                    thread::sleep(Duration::from_secs_f64((timeout / 31.4).max(0.01)))
                }
            }
        }
    }

    /// Schedule the callback to be executed inside the write thread.  
    /// Then wait for it to run.
    ///
    ///     db.write_wait (9., |db| db.exec ("INSERT INTO test (i) VALUES (123)")) .unwrap();
    ///
    pub fn write_wait<F, R>(&self, timeout: f64, mut cb: F) -> Result<R, String>
    where
        F: FnMut(ArcSQLiteHandle) -> Result<R, String> + Send,
        R: Send,
    {
        let mut ret: Option<Result<R, String>> = None;
        let mut done = false;
        let mut cb = |db: ArcSQLiteHandle| {
            ret = Some(cb(db));
            done = true
        };
        let cb: &mut (dyn FnMut(ArcSQLiteHandle) + Send) = &mut cb;
        // Relax the static lifetime requirement because the closure will only run while we live.
        // Relax the `UnwindSafe` requirement because we'll panic if closure panicks.
        let cb: &mut (dyn FnMut(ArcSQLiteHandle) + Send + RefUnwindSafe) = unsafe { transmute(cb) };

        let parker = Parker::new();
        let unparker = parker.unparker().clone();
        let write_wait = Arc::new(Mutex::new(WriteWait {
            alive: true,
            cb,
            cr: None,
            unparker,
        }));
        let mut write_op = WriteOp::ReplicatedWrite(ReplicatedWrite::Scoped(write_wait.clone()));

        /// Mark the scope dead on drop
        struct DropDead {
            write_wait: Arc<Mutex<WriteWait>>,
            skip: bool,
        };
        impl Drop for DropDead {
            fn drop(&mut self) {
                // Skip if already resolved
                if self.skip {
                    return;
                }
                // Panic if poisoned to maintain unwind safety
                let mut write_wait = self.write_wait.lock().expect("!write_wait");
                write_wait.alive = false
            }
        }
        let mut dd = DropDead {
            write_wait,
            skip: false,
        };

        let start = now_float();
        loop {
            match self.writer_tx.try_send(write_op) {
                Ok(()) => break,
                Err(chan::TrySendError::Full(op)) => {
                    if now_float() - start > timeout / 3.14 {
                        return ERR!("Write queue full");
                    }
                    thread::sleep(Duration::from_secs_f64((timeout / 31.4).max(0.01)));
                    write_op = op
                }
                Err(chan::TrySendError::Disconnected(_)) => return ERR!("No writer"),
            }
        }

        loop {
            let passed = {
                // Panic if poisoned to maintain unwind safety
                let mut write_wait = dd.write_wait.lock().expect("!write_wait");
                if done && ret.is_some() {
                    dd.skip = true;
                    return ret.unwrap();
                }
                if let Some(cr) = write_wait.cr.take() {
                    dd.skip = true;
                    match cr {
                        Ok(()) => return ERR!("!ret"),
                        Err(err) => {
                            let panic = any_to_str(&*err).unwrap_or("");
                            // Propagate panics to maintain unwind safety
                            panic!("write_wait cb panic: {}", panic)
                        }
                    }
                }
                let passed = now_float() - start;
                if timeout < passed {
                    write_wait.alive = false; // Cancel the execution
                    dd.skip = true;
                    return ERR!("Timeout");
                }
                passed
            };
            parker.park_timeout(Duration::from_secs_f64((timeout - passed).max(0.01)))
        }
    }
}
